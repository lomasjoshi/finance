(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_minimal-grey',
        radioClass: 'iradio_minimal-grey',
        increaseArea: '20%' // optional
    });

    $('.datepicker').datetimepicker();

    /**
     * for report date range picker
     */
    $('#reportrange').daterangepicker({
        format: 'MM/DD/YYYY',
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        dateLimit: {days: 180},
        showDropdowns: false,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-primary',
        cancelClass: 'btn-default',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Cancel',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
    });


    $('[data-toggle="tooltip"]').tooltip();

    /**
     * panel expanding plugin
     */

    var isExpanded = false;
    var expandDiv = function () {
        isExpanded = true;
        $('#divToCollapse').hide();

        $('#divToExpand').toggleClass($('#divToExpand').data('class') + ' col-lg-12');

        setTimeout(function () {
            $('#divToCollapse').toggleClass($('#divToCollapse').data('class') + ' hide');
            $('#divToCollapse').show();
        }, 1000);
    };

    $(document).on('click', '#expandDiv', function (event) {
        event.preventDefault();

        expandDiv();
    });


    // Delete
    $(document).on('submit', '.delete', function (event) {
        event.preventDefault();

        $this = $(this);

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this record!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                url: $this.attr('action'),
                type: $this.attr('method'),
                dataType: 'json',
                data: $this.serialize()
            })
                .done(function (data) {

                    $("#addEdit .panel-body #formEdit").html('');
                    $("#addEdit .panel-body #formAdd").show();

                    var $title = $('#addEdit .panel-title').data('addlabel');
                    $('#addEdit .panel-title').html('<i class="ion-ios-compose-outline"></i> ' + $title);

                    $this.parents('tr').fadeOut('400', function () {
                        swal("Deleted!", data.response.message, "success");
                        if ($this.parents('tr').siblings().length === 0) {
                            $this.parents('tbody').append('<tr id="noRecord"><td class="alert alert-warning" role="alert" colspan="3">No record to show.</td></tr>');
                        }
                        $this.parents('tr').remove();
                    });
                })
                .fail(function (data) {
                    var $data = $.parseJSON(data.responseText);
                    sweetAlert("Oops...", $data.response.message, "error");
                });


        });
    });

    // branch markup
    var addBranch = function (data) {
        var $response = data;
        return '<tr id="list_' + $response.data.id + '">' +
            '<td>' + $response.data.name + '</td>' +
            '<td class="text-right action">' +
            '<form class="delete" method="POST" action="' + $response.delete_url + '">' +
            '<input name="_method" type="hidden" value="DELETE">' +
            '<input name="_token" type="hidden" value="' + $response._token + '">' +
            '<button class="editLink" data-link="' + $response.edit_url + '"><i class="ion-edit"></i></button> ' +
            '<button type="submit">' +
            '<i class="ion-trash-a"></i>' +
            '</button>' +
            '</form>' +
            '</td>' +
            '</tr>';
    };

    // category markup
    var addCategory = function (data) {
        var $response = data;
        return '<tr id="list_' + $response.data.id + '">' +
            '<td>' + $response.data.name + '</td>' +
            '<td>' + $response.data.parent_name + '</td>' +
            '<td class="text-right action">' +
            '<form class="delete" method="POST" action="' + $response.delete_url + '">' +
            '<input name="_method" type="hidden" value="DELETE">' +
            '<input name="_token" type="hidden" value="' + $response._token + '">' +
            '<button class="editLink" data-link="' + $response.edit_url + '"><i class="ion-edit"></i></button> ' +
            '<button type="submit">' +
            '<i class="ion-trash-a"></i>' +
            '</button>' +
            '</form>' +
            '</td>' +
            '</tr>';
    };


    var addIncome = function (data) {
        var $response = data;
        return '<tr id="list_' + $response.data.id + '">' +
            '<td>' + $response.data.branch_name + '</td>' +
            '<td>' + $response.data.category_name + '</td>' +
            '<td>' + $response.data.amount + '</td>' +
            '<td>' + $response.data.income_date + '</td>' +
            '<td class="text-right action">' +
            '<form class="delete" method="POST" action="' + $response.delete_url + '">' +
            '<input name="_method" type="hidden" value="DELETE">' +
            '<input name="_token" type="hidden" value="' + $response._token + '">' +
            '<button class="editLink" data-link="' + $response.edit_url + '"><i class="ion-edit"></i></button> ' +
            '<button type="submit">' +
            '<i class="ion-trash-a"></i>' +
            '</button>' +
            '</form>' +
            '</td>' +
            '</tr>';
    };

    // ADD
    $(document).on('submit', '.addEdit', function (event) {
        event.preventDefault();
        $this = $(this);
        var $module = $this.data('module');
        var $saveBtn = $this.find('.save');

        var $loadingMessage = $saveBtn.attr('data-loadingMessage');
        var $label = $saveBtn.attr('data-label');

        $saveBtn.html($loadingMessage);

        $saveBtn.addClass('disabled');
        $saveBtn.prop('disabled', true);

        $.ajax({
            url: $this.attr('action'),
            type: $this.attr('method'),
            dataType: 'json',
            data: $this.serialize()
        })
            .done(function (data) {

                var $response = data.response;

                if ($module == 'branch') {
                    $html = addBranch($response);

                } else if ($module == 'category') {

                    $html = addCategory($response);

                    $this.find('#parent_name').html($response.parent_lists);

                } else if ($module == 'incomes') {

                    $html = addIncome($response);

                }

                $('#' + $module + ' #noRecord').remove();

                setTimeout(function () {

                    $saveBtn.removeClass('disabled');
                    $saveBtn.prop('disabled', false);
                    $saveBtn.html($label);

                    if ($response.action === 'Saved!') {

                        $this.trigger("reset");

                        $('#' + $module + ' .table tbody').prepend($html);

                        swal("Saved!", $response.message, "success");

                    } else if ($response.action === 'Changes Saved!') {

                        $("#addEdit .panel-body #formEdit").html('');
                        $("#addEdit .panel-body #formAdd").show();

                        var $title = $('#addEdit .panel-title').data('addlabel');
                        $('#addEdit .panel-title').html('<i class="ion-ios-compose-outline"></i> ' + $title);


                        var $findRowToDelete = $('#' + $module + ' .table tbody').find('#list_' + $response.data.id);

                        $findRowToDelete.fadeOut('fast', function () {
                            $findRowToDelete.remove();
                        });
                        $('#' + $module + ' .table tbody').prepend($html);

                        swal("Saved!", $response.message, "success");

                    } else {
                        sweetAlert("Oops...", "Something went wrong", "error");
                    }


                }, 500);
            })
            .fail(function (data) {
                var $data = $.parseJSON(data.responseText);
                sweetAlert("Oops...", $data.response.message, "error");

                $saveBtn.removeClass('disabled');
                $saveBtn.prop('disabled', false);
                $saveBtn.html($label);
            });

    });


    $(document).on('click', '.editLink', function (event) {
        event.preventDefault();
        $this = $(this);

        var $href = $this.data('link');


        if ($href !== '#') {
            /**
             * trick
             */
            if(isExpanded){
                expandDiv();
                isExpanded = false;
            }

            $("#addEdit .panel-body #formAdd").hide();

            $this.html('<i class="ion-load-c ion-spin-animation"></i>');

            var $siblings = $this.parents('.panel').find('.editLink');
            $.each($siblings, function (index, val) {
                $(this).addClass('disabled');
                $(this).prop('disabled', true);
            });

            $("#addEdit .panel-body #formEdit").load($href, function () {

                $('.datepicker').datetimepicker();

                $("#addEdit").fadeIn("slow", function () {
                    $(this).removeClass('panel-default');
                    $(this).addClass('panel-warning');

                    $this.html('<i class="ion-edit"></i>');
                    $.each($siblings, function (index, val) {
                        $(this).removeClass('disabled');
                        $(this).prop('disabled', false);
                    });

                    var $title = $('#addEdit .panel-title').data('editlabel');
                    $('#addEdit .panel-title').html('<i class="ion-ios-compose-outline"></i> ' + $title);

                    var $windowWidth = $(window).width();
                    if ($windowWidth > 1024) {
                        $('html, body').animate({
                            scrollTop: 0
                        }, 500);
                    }else {

                        $("html, body").animate({ scrollTop: $('#addEdit').offset().top }, 1000);

                    }
                    setTimeout(function () {
                        $("#addEdit").removeClass('panel-warning', {
                            duration: 500
                        });
                        $("#addEdit").addClass('panel-default', {
                            duration: 500
                        });
                    }, 2000);
                });

                $(document).on('click', '.cancel', function (event) {
                    event.preventDefault();

                    $("#addEdit .panel-body #formEdit").html('');
                    $("#addEdit .panel-body #formAdd").show();

                    var $title = $('#addEdit .panel-title').data('addlabel');
                    $('#addEdit .panel-title').html('<i class="ion-ios-compose-outline"></i> ' + $title);
                });

            });
        }

    });


}());
