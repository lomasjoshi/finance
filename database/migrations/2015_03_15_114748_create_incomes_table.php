<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Schema::create('incomes', function (Blueprint $t) {
            $t->increments('id');
            $t->integer('branch_id')->unsigned();
            $t->integer('income_category_id')->unsigned();
            $t->decimal('amount',17);
            $t->date('date');
            $t->timestamps();
            $t->foreign('branch_id')->references('id')->on('branches');
            $t->foreign('income_category_id')->references('id')->on('income_categories');

        });

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');


    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('incomes');
    }

}
