<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model {

	protected $table = 'branches';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

	public function getValidationRules() {
		return [
			'name' => 'required|max:255',
		];
	}
}