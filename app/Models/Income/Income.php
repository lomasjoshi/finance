<?php namespace App\Models\Income;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Income extends Model {

	protected $table = 'incomes';

	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['branch_id', 'income_category_id', 'amount', 'date'];

	protected $dates = [
		'date',
		'income_date',
	];

	public function setDateAttribute($date) {
		$this->attributes['date'] = Carbon::parse($date)->format('Y-m-d');

	}

	public function getDateAttribute($date) {
		return Carbon::parse($date)->format('Y-m-d');

	}

	public function getIncomeDateAttribute($date) {
		return Carbon::parse($date)->toFormattedDateString();

	}
	public function getIncomeAmountAttribute($amount) {
		return number_format($amount, 2);

	}

	public function branch() {
		return $this->belongsTo('App\Models\Branch');
	}

	public function category() {
		return $this->belongsTo('App\Models\Income\Category', 'income_category_id');
	}

	public function getValidationRules() {
		return [
			'branch_id' => 'required',
			'income_category_id' => 'required',
			'amount' => 'required|numeric',
			'date' => 'required|date',
		];

	}

	public function getAllWithRelationalData($searchQuery = '') {
		$model = new static;

		$querybuilding = $model->join('branches', 'incomes.branch_id', '=', 'branches.id')
		                       ->join('income_categories', 'incomes.income_category_id', '=', 'income_categories.id')->select([
			'incomes.id',
			'incomes.income_category_id',
			'income_categories.name as category_name',
			'incomes.amount as income_amount',
			'incomes.date as income_date',
			'branches.name as branch_name',
		]);

		if (!empty($searchQuery['branch'])) {
			$querybuilding->where('branch_id', $searchQuery['branch']);
		}

		if (!empty($searchQuery['category'])) {
			$querybuilding->whereIn('incomes.income_category_id', $searchQuery['category']);
		}

		if (count($searchQuery['date']) === 2) {
			$querybuilding->where('incomes.date', '>=', $searchQuery['date'][0])->where('incomes.date', '<=', $searchQuery['date'][1]);
		}

		return $querybuilding->orderBy('income_date', 'desc')->paginate(20);

	}

	public function findByIdWithRelationalData($id) {
		$model = new static;

		return $model->join('branches', 'incomes.branch_id', '=', 'branches.id')
		             ->join('income_categories', 'incomes.income_category_id', '=', 'income_categories.id')->select([
			'incomes.id',
			'incomes.income_category_id',
			'income_categories.name as category_name',
			'incomes.amount',
			'incomes.date as income_date',
			'branches.name as branch_name',
		])->where('incomes.id', $id)->first();

	}

}