<?php namespace App\Models\Income;

use Baum\Node;

/**
 * IncomeCategory
 */
class Category extends Node
{

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'income_categories';

    /**
     * Get all with it's parent
     *
     * @return mixed
     */
    public function getWithParent()
    {
        return self::leftJoin('income_categories as p', 'income_categories.parent_id', '=', 'p.id')->select([
            'income_categories.id',
            'income_categories.parent_id',
            'income_categories.name',
            'p.name as parent_name'
        ])->get();
    }

    /**
     * Form validation rule
     *
     * @return array
     */
    public function getValidationRules()
    {
        return [
            'name' => 'required|max:255',
        ];
    }

    /**
     * Get nested list array
     *
     * @return Array
     */
    public function getLists()
    {
        return self::getNestedList('name', 'id', '|__');
    }

    public function ancestorsAndSelfAsBreadcumbOf($id)
    {
        $result = '';

        $category = self::find($id,['name']);
        foreach($category->ancestorsAndSelf()->get() as $node)
        {
            $result.= $node->name.'->';
        }

        return rtrim($result,'->');

    }

}
