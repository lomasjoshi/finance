<?php namespace App\Http\Controllers;

use App\Exceptions\FormValidationException;
use App\Http\Requests;
use App\Models\Branch;
use Exception;
use Illuminate\Http\Request;

class BranchesController extends Controller
{
    /**
     * @var Branch
     */
    private $branch;

    /**
     * @param Branch $branch
     */
    public function __construct(Branch $branch)
    {
        $this->middleware('auth');
        $this->branch = $branch;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $branch = $this->branch->paginate(20);

        return view('branch.index')->with('branches', $branch);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('branch.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $this->validateForm($request, $this->branch->getValidationRules());

            $this->branch->fill($input)->save();

            $this->setEditAction(action('BranchesController@edit',[$this->branch->id]));
            $this->setDeleteAction(action('BranchesController@destroy',[$this->branch->id]));

            return $this->respondSaved($this->branch);

        } catch (FormValidationException $e) {

            return $this->setStatusCode($e->getCode())->makeResponse([
                'message' => $e->getMessage()
            ]);

        } catch (Exception $e) {

            return $this->respondUnprocessableEntity([
                'message' => $this->getErrorMessage($e, 'Something went wrong.')
            ]);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $view = view('branch.edit');

        try {
            $branch = $this->branch->findOrFail($id);

            return $view->with('branch', $branch);

        } catch (Exception $e) {

            return $this->respondUnprocessableEntity([
                'message' => $this->getErrorMessage($e, 'Something went wrong.')
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {

            $input = $request->all();
            /**
             * Validating branvh form
             */
            $this->validateForm($request, $this->branch->getValidationRules());

            $branch = $this->branch->findOrFail($id);

            $branch->fill($input)->save();
            $this->setEditAction(action('BranchesController@edit',[$id]));
            $this->setDeleteAction(action('BranchesController@destroy',[$id]));

            return $this->respondChangesSaved($branch);

        } catch (FormValidationException $e) {

            return $this->setStatusCode($e->getCode())->makeResponse([
                'message' => $e->getMessage()
            ]);

        } catch (Exception $e) {

            return $this->respondUnprocessableEntity([
                'message' => $this->getErrorMessage($e, 'Something went wrong.')
            ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try {

            $category = $this->branch->findOrFail($id);

            $category->delete();

            return $this->respondDeleted();

        } catch (Exception $e) {

            return $this->respondUnprocessableEntity([
                'message' => $this->getErrorMessage($e, 'Something went wrong.')
            ]);

        }

    }

}
