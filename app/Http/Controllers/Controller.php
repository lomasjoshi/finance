<?php namespace App\Http\Controllers;

use App\Exceptions\FormValidationException;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Response;
use Symfony\Component\HttpFoundation\Response as SymfonyRequest;

abstract class Controller extends BaseController
{

    use DispatchesCommands, ValidatesRequests;

    private $statusCode = 200;
    private $editAction = '';
    private $deleteAction = '';

    /**
     * @param mixed $statusCode
     *
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }


    /**
     * @param string $message
     *
     * @return mixed
     */
    public function respondNotFound($message = 'Not Found')
    {
        return $this->setStatusCode(SymfonyRequest::HTTP_NOT_FOUND)->makeResponse($message);
    }


    /**
     * @param string $message
     *
     * @return mixed
     */
    public function respondUnprocessableEntity($message = 'Unprocessable Entity')
    {
        return $this->setStatusCode(SymfonyRequest::HTTP_UNPROCESSABLE_ENTITY)->makeResponse($message);
    }


    /**
     * @param string $message
     *
     * @return mixed
     */
    public function respondInternalError($message = 'Internal Server Error')
    {
        return $this->setStatusCode(SymfonyRequest::HTTP_INTERNAL_SERVER_ERROR)->makeResponse($message);
    }


    /**
     * @param string $message
     *
     * @return mixed
     */
    public function respondSuccess($message = '')
    {
        return $this->setStatusCode(SymfonyRequest::HTTP_OK)->makeResponse($message);
    }

    /**
     * @param       $data
     * @param array $headers
     *
     * @return mixed
     */
    private function respond($data, $headers = [])
    {
        return Response::json($data, $this->getStatusCode(), $headers);
    }


    /**
     * @param $message
     *
     * @return mixed
     */
    protected function makeResponse($message)
    {
        return $this->respond([
            'response'   => $message,
            'statusCode' => $this->getStatusCode()
        ]);
    }

    protected function getErrorMessage($exception, $message)
    {
        if (app()->environment() == 'local') {
            return $exception->getMessage();

        }

        return $message;

    }

    public function respondDeleted()
    {
        return $this->respondSuccess([
            'message' => 'Your record has been deleted.'
        ]);

    }

    public function respondChangesSaved($data,$extra = [])
    {
        $response = array_merge([
            'data'    => $data,
            'edit_url' => $this->getEditAction(),
            'delete_url' => $this->getDeleteAction(),
            '_token'  => csrf_token(),
            'action'  => 'Changes Saved!',
            'message' => 'Your data has been saved.'
        ],$extra);
        return $this->respondSuccess($response);


    }

    public function respondSaved($data,$extra = [])
    {
        $response = array_merge([
            'data'     => $data,
            'edit_url' => $this->getEditAction(),
            'delete_url' => $this->getDeleteAction(),
            '_token'   => csrf_token(),
            'action'   => 'Saved!',
            'message'  => 'Your data has been saved.'
        ],$extra);
        return $this->respondSuccess($response);

    }

    /**
     * @param Request $request
     * @param array   $rules
     * @param array   $messages
     *
     * @return bool
     */
    public function validateForm(Request $request, array $rules, array $messages = [])
    {
        $validator = $this->getValidationFactory()->make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            throw new FormValidationException($validator->getMessageBag()->first());
        }

        return true;
    }

    protected function setEditAction($action)
    {
        return $this->editAction = $action;
    }

    protected function getEditAction()
    {
        return $this->editAction;

    }

    protected function setDeleteAction($action)
    {
        return $this->deleteAction = $action;

    }

    protected function getDeleteAction()
    {
        return $this->deleteAction;

    }

}
