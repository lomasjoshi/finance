<?php namespace App\Http\Controllers\Income;

use App\Exceptions\FormValidationException;
use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Income\Category;
use App\Models\Income\Income;
use Illuminate\Http\Request;

class IncomesController extends Controller {
	/**
	 * @var Income
	 */
	private $income;

	/**
	 * @param Income $income
	 */
	public function __construct(Income $income) {

		$this->income = $income;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param Category $category
	 *
	 * @param Branch   $branch
	 *
	 * @param Request  $request
	 *
	 * @return Response
	 */
	public function index(Category $category, Branch $branch, Request $request) {
		$categoryList = $category->getLists();
		$branchList = $branch->lists('name', 'id');
		$categories = '';
		if (!empty($request->query('category'))) {
			$parentCategory = $category->find($request->query('category'));
			if (!empty($parentCategory)) {

				$categories = array_pluck($parentCategory->getdescendantsAndSelf(['id']), 'id');
			}
		}
		$searchQuery = [
			'branch' => $request->get('branch'),
			'category' => $categories,
			'date' => array_map(function ($array) {
				return date('Y-m-d', strtotime($array));
			}, explode('to', $request->get('date_range'))),
		];

		$incomes = $this->income->getAllWithRelationalData($searchQuery);

		return view('income.index', compact('categoryList', 'branchList', 'incomes'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		return view('income.create');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request) {
		try {

			$input = $request->all();

			$this->validateForm($request, $this->income->getValidationRules());

			$this->income->fill($input)->save();

			$this->setEditAction(action('Income\IncomesController@edit', [$this->income->id]));
			$this->setDeleteAction(action('Income\IncomesController@destroy', [$this->income->id]));

			$incomeDetail = $this->income->findByIdWithRelationalData($this->income->id);

			return $this->respondSaved($incomeDetail);

		} catch (FormValidationException $e) {

			return $this->setStatusCode($e->getCode())->makeResponse([
				'message' => $e->getMessage(),
			]);

		} catch (Exception $e) {

			return $this->respondUnprocessableEntity([
				'message' => $this->getErrorMessage($e, 'Something went wrong.'),
			]);
		}

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param Category $category
	 * @param Branch   $branch
	 * @param  int     $id
	 *
	 * @return Response
	 */
	public function edit(Category $category, Branch $branch, $id) {
		$view = view('income.edit');

		try {
			$income = $this->income->findOrFail($id);

			$categoryList = $category->getLists();

			$branchList = $branch->lists('name', 'id');

			return $view->with('income', $income)
			            ->with('categoryList', $categoryList)
			            ->with('branchList', $branchList);

		} catch (Exception $e) {

			return $this->respondUnprocessableEntity([
				'message' => $this->getErrorMessage($e, 'Something went wrong.'),
			]);
		}

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @param  int    $id
	 *
	 * @return Response
	 */
	public function update(Request $request, $id) {
		try {

			$input = $request->all();

			$this->validateForm($request, $this->income->getValidationRules());

			$income = $this->income->findOrFail($id);

			$income->fill($input)->save();

			$this->setEditAction(action('Income\IncomesController@edit', [$id]));

			$this->setDeleteAction(action('Income\IncomesController@destroy', [$id]));
			$incomeDetail = $this->income->findByIdWithRelationalData($income->id);

			return $this->respondChangesSaved($incomeDetail);

		} catch (FormValidationException $e) {

			return $this->setStatusCode($e->getCode())->makeResponse([
				'message' => $e->getMessage(),
			]);

		} catch (Exception $e) {

			return $this->respondUnprocessableEntity([
				'message' => $this->getErrorMessage($e, 'Something went wrong.'),
			]);
		}

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id) {
		try {

			$income = $this->income->findOrFail($id);

			$income->delete();

			return $this->respondDeleted();

		} catch (Exception $e) {

			return $this->respondUnprocessableEntity([
				'message' => $this->getErrorMessage($e, 'Something went wrong.'),
			]);

		}

	}

}