<?php namespace App\Http\Controllers\Income;

use App\Exceptions\FormValidationException;
use App\Http\Controllers\Controller;
use App\Models\Income\Category;
use Exception;
use Illuminate\Http\Request;

class CategoriesController extends Controller {

	/**
	 * @var Category
	 */
	private $category;

	/**
	 * @param Category $category
	 */
	public function __construct(Category $category) {
		$this->middleware('auth');
		$this->category = $category;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$categories = $this->category->getWithParent();

		$categoriesList = $this->category->getLists();

		return view('income.category.index', compact('categories', 'categoriesList'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		return view('income.categories.create');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request) {
		try {
			$parentDetail = [];

			$input = $request->all();

			$this->validateForm($request, $this->category->getValidationRules());

			$this->category->fill($input)->save();

			if (!empty($input['parent_id'])) {

				$this->category->makeChildOf($input['parent_id']);
				$parentDetail = $this->category->parent()->select(['name as parent_name'])->first()->toArray();
			}

			$this->setEditAction(action('Income\CategoriesController@edit', [$this->category->id]));

			$this->setDeleteAction(action('Income\CategoriesController@destroy', [$this->category->id]));

			$categoriesList = $this->category->getLists();

			$categoryFinal = array_merge($this->category->toArray(), $parentDetail);

			return $this->respondSaved($categoryFinal, [
				'parent_lists' => view('income.category.option', compact('categoriesList'))->render(),
			]);

		} catch (FormValidationException $e) {

			return $this->setStatusCode($e->getCode())->makeResponse([
				'message' => $e->getMessage(),
			]);

		} catch (Exception $e) {

			return $this->respondUnprocessableEntity([
				'message' => $this->getErrorMessage($e, 'Something went wrong.'),
			]);
		}

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id) {
		$view = view('income.category.edit');

		try {
			$category = $this->category->findOrFail($id);

			$categoriesList = $this->category->getlists();

			$view->with('categoriesList', $categoriesList);

			return $view->with('category', $category);

		} catch (Exception $e) {

			return $this->respondUnprocessableEntity([
				'message' => $this->getErrorMessage($e, 'Something went wrong.'),
			]);
		}

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @param  int    $id
	 *
	 * @return Response
	 */
	public function update(Request $request, $id) {
		try {

			$parentDetail = [];

			$input = $request->all();

			$this->validateForm($request, $this->category->getValidationRules());

			$category = $this->category->findOrFail($id);

			$category->fill($input)->save();

			if (!empty($input['parent_id'])) {

				$category->makeChildOf($input['parent_id']);
				$parentDetail = $category->parent()->select(['name as parent_name'])->first()->toArray();
			} else {
				$category->makeRoot();
			}
			$categoriesList = $this->category->getLists();

			$this->setEditAction(action('Income\CategoriesController@edit', [$id]));

			$this->setDeleteAction(action('Income\CategoriesController@destroy', [$id]));

			$categoryFinal = array_merge($category->toArray(), $parentDetail);

			return $this->respondChangesSaved($categoryFinal, [
				'parent_lists' => view('income.category.option', compact('categoriesList'))->render(),
			]);

		} catch (FormValidationException $e) {

			return $this->setStatusCode($e->getCode())->makeResponse([
				'message' => $e->getMessage(),
			]);

		} catch (Exception $e) {

			return $this->respondUnprocessableEntity([
				'message' => $this->getErrorMessage($e, 'Something went wrong.'),
			]);
		}

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id) {
		try {

			$category = $this->category->findOrFail($id);

			$category->delete();

			return $this->respondDeleted();

		} catch (Exception $e) {

			return $this->setStatusCode($e->getCode())->makeResponse([
				'message' => $this->getErrorMessage($e, 'Something went wrong.'),
			]);
		}

	}

}