<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');

Route::group(['namespace' => 'Income', 'prefix' => 'income'], function () {

	Route::resource('categories', 'CategoriesController');
	Route::get('/', 'IncomesController@index');
	Route::get('create', 'IncomesController@create');
	Route::post('store', 'IncomesController@store');
	Route::get('{id}/edit', 'IncomesController@edit');
	Route::put('{id}', 'IncomesController@update');
	Route::delete('{id}', 'IncomesController@destroy');

});
Route::group(['prefix' => 'config'], function () {

	Route::resource('branches', 'BranchesController');

});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
