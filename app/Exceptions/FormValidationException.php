<?php
/**
 * wlink-finance
 * User: Amrit
 * Date: 3/12/15
 * Email: music.demand01@gmail.com
 * Website: http://amritgc.com.np
 * Time: 10:24 AM
 */

namespace App\Exceptions;


use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class FormValidationException extends UnprocessableEntityHttpException{

    public function __construct($message = null)
    {
        parent::__construct($message, null, 422);
    }

}