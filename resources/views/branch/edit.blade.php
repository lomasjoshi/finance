{!! Form::model($branch,['method'=>'PUT','class'=>'addEdit','data-module'=>'branch','action'=> ['BranchesController@update',$branch->id]]) !!}
    @include('branch.form')
    <button type="submit" class="btn btn-primary save" data-loadingMessage="Saving..." data-label="Save">Save</button>
    <button class="btn btn-danger cancel" data-label="Cancel">Cancel</button>
{!! Form::close() !!}