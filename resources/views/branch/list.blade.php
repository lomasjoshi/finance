@forelse($branches as $branch)
    <tr id="list_{{ $branch->id }}">
        <td> {{ $branch->name }}</td>
        <td class="text-right action">
            {!! Form::open(['class'=>'delete','method'=>'DELETE','action'=>['BranchesController@destroy',$branch->id]]) !!}

            <button class="editLink" data-link="{{ action('BranchesController@edit',[$branch->id]) }}"><i class="ion-edit"></i></button>
            <button type="submit"><i class="ion-trash-a"></i></button>
            {!! Form::close() !!}
        </td>
    </tr>
@empty
<tr id="noRecord">
    <td class="alert alert-warning" role="alert" colspan="3">No record to show.</td>
</tr>
@endforelse
