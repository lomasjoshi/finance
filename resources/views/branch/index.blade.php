@extends('layouts.master')
@section('content')
<div class="col-md-7">
    <div class="panel panel-default" id="branch">
        <div class="panel-heading">
            <div class="panel-title"><i class="ion-ios-list-outline"></i> Branch's Lists</div>
        </div>
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Branch Name</th>
                    <th class="text-right">Action</th>
                </tr>
                </thead>
                <tbody>

                @include('branch.list')

                </tbody>
            </table>
            {!! $branches->render() !!}
        </div>
    </div>
</div>
    <div class="col-md-5">
        <div class="panel panel-default" id="addEdit">
            <div class="panel-heading">
                <h3 class="panel-title" data-addlabel="New Branch" data-editlabel="Update Branch"><i class="ion-ios-compose-outline"></i> New Branch</h3>
            </div>
            <div class="panel-body">
                <div id="formAdd">
                    {!! Form::open(['class'=>'addEdit','data-module'=>'branch','action'=>'BranchesController@store']) !!}
                        @include('branch.form')
                    <button type="submit" class="btn btn-primary save" data-loadingMessage="Saving..." data-label="Save">Save</button>
                    {!! Form::close() !!}
                </div>
                <div id="formEdit"></div>

            </div>
        </div>
    </div>

    @stop
