<div class="form-group">
    <label for="branch_name">Name </label>
    {!! Form::text('name',null,[
            'class'         => "form-control",
            'id'            => "branch_name",
            'placeholder'   => "Enter branch name"
            ])
    !!}
</div>