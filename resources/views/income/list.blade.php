@forelse($incomes as $income)
    <tr id="list_{{ $income->id }}">
        <td> {{ $income->branch_name }}</td>
        <td> {{ $income->category_name }}</td>
        <td> {{ $income->income_amount }}</td>
        <td> {{ $income->income_date }}</td>

        <td class="text-right action">

            {!! Form::open(['class'=>'delete','method'=>'DELETE','action'=>['Income\IncomesController@destroy',$income->id]]) !!}

            <button class="editLink" data-link={{ action('Income\IncomesController@edit',[$income->id]) }}><i class="ion-edit"></i></button>

            <button type="submit"><i class="ion-trash-a"></i></button>

            {!! Form::close() !!}
        </td>
    </tr>
@empty
    <tr id="noRecord">
        <td class="alert alert-warning" role="alert" colspan="5">No record to show.</td>
    </tr>
@endforelse