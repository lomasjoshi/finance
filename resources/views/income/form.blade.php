<div class="form-group">
    <label for="branch">Branch </label>
    {!! Form::select('branch_id',[''=>'Choose branch']+$branchList,null,[
        'class'         => "form-control",
        'id'            => "branch",
        ])
    !!}
</div>

<div class="form-group">
    <label for="parent_name">Income Category </label>
    {!! Form::select('income_category_id',[''=>'Choose income Category']+$categoryList,null,[
        'class'         => "form-control",
        'id'            => "parent_name",
        ])
    !!}
</div>

<div class="form-group">
    <label for="amount">Amount </label>
    {!! Form::text('amount',null,[
        'class'         => "form-control",
        'id'            => "amount",
        'placeholder'   => "Enter income amount"
        ])
    !!}
</div>

<div class="form-group">
    <label for="date">Date </label>
    {!! Form::text('date',null,[
        'class'         => "form-control datepicker",
        'id'            => "date",
        'data-date-format'=>'YYYY-MM-DD',
        'placeholder'   => "Enter income date"
        ])
    !!}
</div>