@extends('layouts.master')

@section('content')
<div class="col-lg-12">
    <div class="panel panel-default"  id="filter_incomes">
        <div class="panel-heading">
            <div class="panel-title">
                <i class="ion-arrow-graph-up-right"></i> Filter Incomes
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <form>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="ion-ios-briefcase"></i>
                                    </div>

                                    {!! Form::select('branch',[''=>'Choose branch']+$branchList,Input::query('branch'),[
                                        'class'         => "form-control",
                                        'id'            => "branch",
                                        ])
                                    !!}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="ion-ios-list"></i>
                                    </div>

                                    {!! Form::select('category',[''=>'Choose income Category']+$categoryList,Input::query('category'),[
                                        'class'         => "form-control",
                                        'id'            => "parent_name",
                                        ])
                                    !!}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="ion-calendar"></i>
                                    </div>
                                    {!! Form::text('date_range',Input::query('date_range'),[
                                        'class' =>'form-control',
                                        'id'    => "reportrange",
                                        ])
                                    !!}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-1">
                            <button type="submit" class="btn btn-primary btn-block">
                                Filter
                            </button>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-9" data-class="col-lg-9" id="divToExpand">
    <div class="panel panel-default"  id="incomes">
        <div class="panel-heading">
            <div class="panel-title pull-left">
                <i class="ion-arrow-graph-up-right"></i> Income Log
            </div>
            <div class="visible-lg">
                <div class="pull-right" id="expandDiv" data-toggle="tooltip" data-placement="top"
                     title="Click to expand/collapse"><i class="ion-android-expand"></i></div>
            </div>
            <div class="clearfix"></div>

        </div>
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Branch</th>
                    <th>Category</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th class="text-right">Action</th>
                </tr>
                </thead>
                <tbody>

                @include('income.list')

                </tbody>
            </table>
            {!! $incomes->appends(Input::query())->render() !!}

        </div>
    </div>
</div>

<div class="col-lg-3" data-class="col-lg-3" id="divToCollapse">
    <div class="panel panel-default" id="addEdit">
        <div class="panel-heading">
            <h3 class="panel-title" data-addlabel="New Income" data-editlabel="Update Income"><i class="ion-ios-compose-outline"></i> New Income</h3>
        </div>
        <div class="panel-body">
            <div id="formAdd">
                {!! Form::open(['class'=>'addEdit','data-module'=>'incomes','action'=>'Income\IncomesController@store']) !!}
                    @include('income.form')
                <button type="submit" class="btn btn-primary save" data-loadingMessage="Saving..." data-label="Save">Save</button>
                {!! Form::close() !!}
            </div>
            <div id="formEdit"></div>
        </div>
    </div>
</div>
@stop