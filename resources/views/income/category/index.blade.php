@extends('layouts.master')
@section('content')
    <div class="col-md-7">
        <div class="panel panel-default" id="category">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="ion-ios-list-outline"></i> Income Categories Lists
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Parent</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    @include('income.category.list')

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="panel panel-default" id="addEdit">
            <div class="panel-heading">
                <h3 class="panel-title" data-addlabel="New Category" data-editlabel="Update Category"><i class="ion-ios-compose-outline"></i> New Category</h3>
            </div>
            <div class="panel-body">
                <div id="formAdd">
                    {!! Form::open(['class'=>'addEdit','data-module'=>'category','action'=>'Income\CategoriesController@store']) !!}
                    @include('income.category.form')
                    <button type="submit" class="btn btn-primary save" data-loadingMessage="Saving..." data-label="Save">Save</button>
                    {!! Form::close() !!}
                </div>
                <div id="formEdit"></div>
            </div>
        </div>
    </div>

@stop
