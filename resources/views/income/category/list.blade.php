@forelse($categories as $category)
    <tr id="list_{{ $category->id }}">
        <td> {{ $category->name }}</td>
        <td> {{ $category->parent_name }}</td>

        <td class="text-right action">

            {!! Form::open(['class'=>'delete','method'=>'DELETE','action'=>['Income\CategoriesController@destroy',$category->id]]) !!}

            <button class="editLink" data-link={{ action('Income\CategoriesController@edit',[$category->id]) }}><i class="ion-edit"></i></button>

            <button type="submit"><i class="ion-trash-a"></i></button>

            {!! Form::close() !!}
        </td>
    </tr>
@empty
    <tr id="noRecord">
        <td class="alert alert-warning" role="alert" colspan="3">No record to show.</td>
    </tr>
@endforelse