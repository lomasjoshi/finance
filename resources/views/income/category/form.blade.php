<div class="form-group">
    <label for="parent_name">Parent Category </label>
    {!! Form::select('parent_id',[0=>'Choose parent Category']+$categoriesList,null,[
        'class'         => "form-control",
        'id'            => "parent_name",
        ])
    !!}
</div>
<div class="form-group">
    <label for="category_name">Name </label>
    {!! Form::text('name',null,[
        'class'         => "form-control",
        'id'            => "category_name",
        'placeholder'   => "Enter income category name"
        ])
    !!}
</div>