<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bee Finance</title>

    <!-- all vendor css -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:600' rel='stylesheet' type='text/css'>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;lang=en" rel="stylesheet">

    <link href="/css/vendor.css" rel="stylesheet">
    <!-- all our css -->
    <link href="/css/style.css" rel="stylesheet">

    {{-- just for fun will ask kishan da --}}
    <style type="text/css">
        div.panel {
            box-shadow: 0px 1px #C8C8C8;
        }

        .navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:focus, .navbar-default .navbar-nav>.active>a:hover {
            box-shadow: 0 1px #c8c8c8;
        }
        .navbar-default {
            box-shadow: 0 1px #C8C8C8;
        }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="color-line"></div>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <span class="text">{ BeeFinance }</span>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            @if (!Auth::guest())
            <ul class="nav navbar-nav">
                <li><a href="#"><i class="ion-ios-speedometer-outline"></i> Dashboard</a></li>
                <li class="{{ Request::is('config/branches*') ? 'active' : '' }}"><a href="{{ action('BranchesController@index') }}"><i class="ion-ios-briefcase-outline"></i> Branch</a></li>
                <li class="dropdown {{ Request::is('income*') ? 'active' : '' }}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                                class="ion-arrow-graph-up-right"></i> Income
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ action('Income\IncomesController@index') }}">Manage Income</a></li>
                        <li><a href="{{ action('Income\CategoriesController@index') }}">Category</a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="ion-ios-pie-outline"></i> Reports</a></li>

            </ul>
            @endif
            <ul class="nav navbar-nav navbar-right">

                @if (!Auth::guest())
                    <li><a href="{{ url('/auth/logout') }}"><i class="ion-android-exit"></i> Logout</a></li>
                @endif
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<div class="wrapper container">
    <div class="row">
        @section('content')
    </div>
    @show
</div>
<script src="/js/vendor.js" type="text/javascript"></script>
<script src="/js/script.js" type="text/javascript"></script>
</body>
</html>